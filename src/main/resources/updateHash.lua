local hgetall = function (key)
    local bulk = redis.call('HGETALL', key)
    local result = {}
    local nextkey
    for i, v in ipairs(bulk) do
        if i % 2 == 1 then
            nextkey = v
        else
            result[nextkey] = v
        end
    end
    return result
end
local provider = hgetall(KEYS[1])

--if provider doesnt exists then create a new one
if provider['first'] == nil or provider['first'] == '' then
    redis.call('HSET', KEYS[1],'first',ARGV[1])
    redis.call('HSET', KEYS[1],'last',ARGV[2])
    redis.call('HSET', KEYS[1],'facilityId',ARGV[3])
else
    -- provider exists just update facility id
    redis.call('HSET', KEYS[1],'facilityId',ARGV[3] .. 'x')
end


function table.empty (self)
    for _, _ in pairs(self) do
        return false
    end
    return true
end
