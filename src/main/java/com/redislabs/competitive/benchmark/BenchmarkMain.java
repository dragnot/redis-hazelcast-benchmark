package com.redislabs.competitive.benchmark;

import com.google.common.base.Stopwatch;
import com.redislabs.competitive.benchmark.clients.HazlecastClient;
import com.redislabs.competitive.benchmark.clients.RedisClient;
import com.redislabs.competitive.benchmark.entities.Provider;
import com.redislabs.competitive.benchmark.load.BenchmarkLoader;
import org.apache.commons.math3.stat.Frequency;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import static java.lang.System.exit;
import static java.lang.System.out;

/**
 * Created by guylubovitch on 10/22/16.
 */
public class BenchmarkMain {

    final String _host;
    final int _port;
    final String _db_name;
    final int _iteration;
    final int _run_time_in_sec;
    final int _threads;
    final AtomicLong _benchmarkCounter = new AtomicLong(0);
    private final String _operation_type;
    private final int _bulk_size;
    private boolean stopTest=false;
    private long _lastTimestamp=0;
    private List<String> _accessKeys;
    private final String KEY_PREFIX ;
    private final String _filepath;
    private final  Queue<Long> _latencyQueue = new ConcurrentLinkedQueue<>();
    private final Queue<Double> _tpsQueue = new ConcurrentLinkedQueue<>();

    //Constants
    private static final String txgetset =  "txgetset";
    private static final String getFirstname =  "getFirstname";
    private static final String getTextFirstname =  "getTextFirstname";

    private static final String first_prefix =  "first";
    private static final String last_prefix =  "last";
    private static final String facilityId =  "facilityId";




    public static void main(String[] args) throws IOException {


        // this will just do a load process
        if ( args != null && args.length == 1 && args[0].equalsIgnoreCase("L")  )
        {
            out.println("****************************************************************");
            out.println("******** Just doing load process      *******************");

            BenchmarkLoader loader = new BenchmarkLoader();

            loader.loadJsonToDB();

            out.println("******** complete load process      *******************");

            exit(1);

        }


        out.println("****************************************************************");
        out.println("******** Starting benchmark                 *******************");

        BenchmarkMain benchmarkMain = new BenchmarkMain();
        benchmarkMain.prepareData();
        benchmarkMain.warmUp();
        benchmarkMain.runBenchmark();

        /******* runBenchmark the db that you want   ****************/


    }

    /**
     * get the db and the client warmed up through simple set and get operation
     */
    private void warmUp() {

        out.println("Starting warm up ");
        //get db client instance  based on DB name
        KeyValueStore db = getDB();

        String key = "justwarmupKey";
        String value  = "justwarmupValue";

        for (int i = 0; i < 50000; i++) {
            Provider provider = new Provider(key + i,"dummy" + i,"dummy" + i,"dummy" + i);
            db.set(key + i,provider);
            db.get(key + i);
        }
        out.println("Completed warm up ");
    }

    /**
     * this method will prepare a list of keys to access when doing benchmark
     */
    private void prepareData() {


        if ( _operation_type.equalsIgnoreCase(txgetset) ) {
            //add iteration to search for all keys
            for (int i = 0; i < _iteration; i++) _accessKeys.add(KEY_PREFIX + i);

        }
        else if (_operation_type.equalsIgnoreCase(getFirstname))
            _accessKeys =  Utils.getSearchFirst(KEY_PREFIX,_filepath);
        else if (_operation_type.equalsIgnoreCase(getTextFirstname))
            _accessKeys =  Utils.getSearchFirst(KEY_PREFIX,_filepath);


    }

    /**
     *
      */
    private void runBenchmark() {

        //key and value prefix
        //this value is similar the size of 100 in memtier benchmark
        //perform write benchmark
        final Thread[] benchmarkThreads = new Thread[_threads];
        final CountDownLatch latch = new CountDownLatch(_threads);
        final int bulk = 1;
        //set the timestamp for the first run
        _lastTimestamp = System.currentTimeMillis();
        String value = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";

        //get db client instance  based on DB name
        KeyValueStore db = getDB();
        int interval  =100000;
        System.out.println("Current sampling interval " + interval + " start number of threads = " + _threads);
        Stopwatch totalRun = Stopwatch.createStarted();
        //runBenchmark load on multiple threads
        //for (Thread thread: benchmarkThreads) {
        for (int i = 0; i < benchmarkThreads.length; i++) {

            Runnable task = () -> {


                int currentCounter=0;
                while (true)
                {
                    //check if to stop loop
                    if ( stopTest ) {
                        latch.countDown();
                        break;
                    }
                    //increase iteration and reset if its bigger then max
                    if ( ++currentCounter == _accessKeys.size())
                        currentCounter=0;


                    //prepare key
                    String key = null;
                    try {
                        key = _accessKeys.get(currentCounter);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    //create input values
                    performOperation(db,key,value);


                    if ( (_benchmarkCounter.incrementAndGet() % interval) == 0 )
                    {
                        //calculate TPS
                        calculateTPS(interval,bulk );
                    }


                }
                latch.countDown();
            };

            benchmarkThreads[i] = new Thread(task);
            benchmarkThreads[i].start();

        }
        //stop threads when time is done
        while (true)
        {

            if ( (totalRun.elapsed(TimeUnit.SECONDS) % 10) == 0 )
                out.println("Finished testing after " + totalRun.elapsed(TimeUnit.SECONDS));

            if ( totalRun.elapsed(TimeUnit.SECONDS)  > _run_time_in_sec ) {
                stopTest = true;
                out.println("Finished testing after " + totalRun.elapsed(TimeUnit.SECONDS));
                break;
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        /** wait for all threads to complete ****/
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        calculateTotalTPS();

        calculateHistogram();



        System.out.println("Done!");

    }

    private void calculateTotalTPS() {

        List<Double> allTPS =  new ArrayList<>(_tpsQueue);
        DoubleSummaryStatistics stats = allTPS.stream()
                .mapToDouble((x) -> x)
                .summaryStatistics();

        System.out.println("avg TPS  : "  + stats);

    }

    private void calculateHistogram() {
        //calculate latency histogram
        Frequency frequency = new Frequency();
        _latencyQueue.forEach(frequency::addValue);

        for (long i = 0; i < 50; i++) {
            System.out.println("latency of " + i + " count " + frequency.getCount(i));
        }
    }

    private void performOperation(KeyValueStore db,String key,String value) {


        Long start = System.currentTimeMillis();
        //check the latency for each operation and create histogram
        performSingleKeyOperation(db,key,value);
        //add
        long latency = System.currentTimeMillis() - start;
//        System.out.println("Current Latency " + latency);
        _latencyQueue.add( latency );


    }

    private void performSingleKeyOperation(KeyValueStore db, String key, String value) {


        if ( _operation_type.equalsIgnoreCase(getFirstname) )
        {
            db.getMatchingFirst(key);

        }
        else if (_operation_type.equalsIgnoreCase(getTextFirstname))
        {
            String partialFirst;
            //contact the first chars
            if ( key != null && key.length() > 2 )
                partialFirst = key.substring(0,2);
            else
                partialFirst = key;

            db.getTextMatchFirst(partialFirst);
        }
        else if (_operation_type.equalsIgnoreCase(txgetset))
        {
            Provider provider = new Provider();
            provider.setResourceId(key);
            provider.setFirstname(first_prefix + value);
            provider.setLastname(last_prefix + value);
            provider.setFacilityId(facilityId + value);
            db.txSetGet(provider);
        }



    }

    private KeyValueStore getDB() {


        KeyValueStore db=null;
        if ( _db_name.equalsIgnoreCase("redis") )
            db = new RedisClient(_host,_port);
        else if (_db_name.equalsIgnoreCase("hazelcast"))
            db = new HazlecastClient(_host,_port);

        //flush DB before we runBenchmark benchmark
        return db;

    }

    /**
     * calculate the TPS Of the current method
     * @param counter
     * @param bulk
     */
    private void calculateTPS(long counter, int bulk) {

        long current = System.currentTimeMillis();
        long totalRun = current -  _lastTimestamp;

        double tps  = ((double)counter/(double)totalRun) * (1000 * bulk);
        //set the last timestamp as the current one
        _lastTimestamp = current;
        System.out.println("total TPS is " + tps);
        _tpsQueue.add(tps);
    }

    /**
     *
     * @throws IOException
     */
    public BenchmarkMain() throws IOException {


        /**** set arguments **************************/
        String propPath = System.getProperty( "application.properties" );
        Properties prop = new Properties();
        // load properties file
        prop.load(new FileInputStream(propPath));
        // get properties
        _host = prop.getProperty("host","localhost");
        _port = Integer.parseInt(prop.getProperty("port","6379"));
        _threads = Integer.parseInt(prop.getProperty("threads","1"));
        _iteration = Integer.parseInt(prop.getProperty("iteration","100000"));
        _run_time_in_sec = Integer.parseInt(prop.getProperty("run_time_in_sec","10"));
        _db_name = prop.getProperty("db_name","redis");
        _operation_type = prop.getProperty("operation_type","set");
        _bulk_size = Integer.parseInt(prop.getProperty("bulk_size","1"));
        KEY_PREFIX = prop.getProperty("prefix","bench");
        _filepath = prop.getProperty("file","/Users/guylubovitch/Dropbox/redislabds/customers/kaiser/SampleData.csv");

        _accessKeys = new ArrayList<>();


    }

}
