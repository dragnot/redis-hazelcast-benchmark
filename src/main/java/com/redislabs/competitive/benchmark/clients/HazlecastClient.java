package com.redislabs.competitive.benchmark.clients;

import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.query.*;
import com.redislabs.competitive.benchmark.KeyValueStore;
import com.redislabs.competitive.benchmark.entities.Provider;

import java.util.Collection;


/**
 * Created by guylu on 9/26/2016.
 */
public class HazlecastClient implements KeyValueStore {

    private final HazelcastInstance instance;
    private final  IMap<String,Provider> map;

    public HazlecastClient(String host, int port){

        ClientConfig clientConfig = new ClientConfig();


        clientConfig.getNetworkConfig().addAddress(host+ ":" + Integer.toString(port));

        instance = HazelcastClient.newHazelcastClient(clientConfig);
        //complex object map
        map =  instance.getMap("provider");
        //simple object man

        System.out.println("Starting Hazelcast client");


    }

    @Override
    public void flush() {
        map.flush();
    }

    @Override
    public void set(String key, Provider value) {
        map.set(key, value);
    }

    @Override
    public Provider get(String key) {

        return map.get(key);
    }

    @Override
    public void txSetGet(Provider provider) {


//        //lock key to make sure no one is changing it
        map.lock(provider.getResourceId());
//        //get key
        Provider response = map.get(provider.getResourceId());
        if ( response == null )
            map.set(provider.getResourceId(),provider);
        else
            provider.setFacilityId( provider.getFacilityId() + "X" );
            map.set(provider.getResourceId(),provider);
        //unlock key
        map.unlock(provider.getResourceId());


    }

    public static void main(String[] args) {

        HazlecastClient hazlecastClient = new HazlecastClient("10.0.10.94",5701);

        String firstName = "GOERGEXX";

        if ( args.length > 0 ) {
            Provider provider = new Provider();
            provider.setResourceId("11111");
            provider.setFirstname(firstName);
            provider.setLastname("LLLAAA");
            hazlecastClient.add(provider);
            Provider p =  hazlecastClient.getProvider("11111");
            return;
        }

            Provider provider1  = hazlecastClient.getMatchingFirst(firstName);



    }

    @Override
    public void add(Provider provider) {

        map.set(provider.getResourceId(),provider);

    }

    public Provider getProvider(String id)
    {
        return map.get(id);
    }
    @Override
    public Provider getMatchingFirst(String first) {



        EntryObject e = new PredicateBuilder().getEntryObject();
        Predicate predicate =  e.get( "firstname" ).equal( first ) ;
        Collection<Provider> providers =  map.values(predicate);

        if ( providers != null && providers.size() >= 1 )
            return providers.iterator().next();
        else
            System.out.println(" error no provider was found ");

        return null;
    }

    @Override
    public Provider getTextMatchFirst(String first_chars) {

        /**** this doesnt seem to work or return results ****/
        EntryObject e = new PredicateBuilder().getEntryObject();
        Provider provider  = new Provider();
        provider.setFirstname(first_chars);
        Predicate predicate = e.get( "firstname" ).greaterEqual  (provider);
        //Predicate likeP = Predicates.ilike( "firstname", first_chars );
        PagingPredicate pagingPredicate = new PagingPredicate( predicate , 5 );
        Collection<Provider> providers = map.values(pagingPredicate);
        /*********/
        //Collection<Provider> providers = map.values( new SqlPredicate( " firstname like " + first_chars + "%"  ) );



        if ( providers != null && providers.size() > 1 )
            return providers.iterator().next();
        else
            System.out.println(" error no provider was found ");

        return null;
    }

    @Override
    public long size() {
        return map.size();
    }

}
