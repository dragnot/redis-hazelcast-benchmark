package com.redislabs.competitive.benchmark.clients;

import com.redislabs.competitive.benchmark.KeyValueStore;
import com.redislabs.competitive.benchmark.entities.Provider;
import redis.clients.jedis.*;

import java.util.*;

/**
 * Created by guylubovitch on 10/22/16.
 */
public class RedisClient implements KeyValueStore {

    private final JedisPool _pool;
    private String _update_sh;
    private String[] keys  = new String[3];
    private String[] values  = new String[3];


    private final String UPDATE_LUA = "local hgetall = function (key)\n" +
            "    local bulk = redis.call('HGETALL', key)\n" +
            "    local result = {}\n" +
            "    local nextkey\n" +
            "    for i, v in ipairs(bulk) do\n" +
            "        if i % 2 == 1 then\n" +
            "            nextkey = v\n" +
            "        else\n" +
            "            result[nextkey] = v\n" +
            "        end\n" +
            "    end\n" +
            "    return result\n" +
            "end\n" +
            "local provider = hgetall(KEYS[1])\n" +
            "\n" +
            "--if provider doesnt exists then create a new one\n" +
            "if provider['first'] == nil or provider['first'] == '' then\n" +
            "    redis.call('HSET', KEYS[1],'first',ARGV[1])\n" +
            "    redis.call('HSET', KEYS[1],'last',ARGV[2])\n" +
            "    redis.call('HSET', KEYS[1],'facilityId',ARGV[3])\n" +
            "else\n" +
            "    -- provider exists just update facility id\n" +
            "    redis.call('HSET', KEYS[1],'facilityId',ARGV[3] .. 'x')\n" +
            "end\n";


    public RedisClient(String host, int port)
    {
        JedisPoolConfig poolConfig=new JedisPoolConfig();

        // defaults to make your life with connection _pool easier :)
        poolConfig.setTestWhileIdle(false);
        poolConfig.setTestOnBorrow(false);
        poolConfig.setTestOnReturn(false);
        poolConfig.setMinEvictableIdleTimeMillis(60000);
        poolConfig.setTimeBetweenEvictionRunsMillis(30000);
        poolConfig.setNumTestsPerEvictionRun(-1);
        poolConfig.setMaxTotal(200);

        _pool = new JedisPool(poolConfig, host , port,10000);

        //load LUA script
        Jedis jedis = _pool.getResource();
        _update_sh =jedis.scriptLoad(UPDATE_LUA);
        jedis.close();


    }


    @Override
    public void add(Provider provider) {

        keys[0] = "first";
        keys[1] = "last";
        values[0] = provider.getFirstname();
        values[1] = provider.getLastname();

        if ( provider.getFacilityId() != null )
        {
            keys[2] = "FACILITY_ID";
            values[2] = provider.getFacilityId();

        }

        Jedis jedis = _pool.getResource();

        Map<String,String> map = new HashMap<>();

        for (int i = 0; i < keys.length; i++) {
            map.put(keys[i],values[i]);
        }
        Pipeline pipeline = jedis.pipelined();

        //add provider to redis
        pipeline.hmset(provider.getResourceId(),map);
        // add first name range index
        pipeline.zadd("first_range",0,provider.getFirstname() + ":" + provider.getResourceId());
        // add resource id to this first name
        pipeline.sadd(provider.getFirstname() + ":index",provider.getResourceId());
        pipeline.sync();
        jedis.close();


    }

    @Override
    public Provider getMatchingFirst(String first) {

        Jedis jedis = _pool.getResource();

        Set<String> resourceIds =  jedis.smembers(first + ":index");

        Pipeline pipeline = jedis.pipelined();

        for (String resourceId :
                resourceIds) {
            pipeline.hgetAll(resourceId);
        }

        List<Object> allData = pipeline.syncAndReturnAll();


        jedis.close();
        return new Provider();
    }

    @Override
    public Provider getTextMatchFirst(String first_chars) {

        Jedis jedis = _pool.getResource();


        Set<String> results =  jedis.zrangeByLex("first_range","[" + first_chars,"+",0,1 );
        List<String> resourceIds = new ArrayList<>();
        for (String result:results
             ) {
            String[] items = result.split(":");
            resourceIds.add(items[1]);
        }

        Pipeline pipeline = jedis.pipelined();

        for (String resourceId:resourceIds
             ) {
            pipeline.hgetAll(resourceId);
        }

        List<Object> providers = pipeline.syncAndReturnAll();

        jedis.close();
        //get the first provider in the list
        Provider provider = null;
        if ( resourceIds.size() > 0 ) {

            HashMap<String,String> providerHash = (HashMap) providers.get(0);
            provider = new Provider(resourceIds.get(0).toString(),providerHash.get("first"),providerHash.get("last"), providerHash.get("FACILITY_ID"));
        }
        else
            System.out.println("error no provider was found ");


        return provider;
    }

    @Override
    public long size() {
        Jedis jedis  = _pool.getResource();
        long size = jedis.dbSize();
        jedis.close();
        return size;
    }

    @Override
    public void flush() {
        Jedis jedis = _pool.getResource();
        jedis.flushDB();
        jedis.close();
    }

    @Override
    public void set(String key, Provider value) {


        Jedis jedis = _pool.getResource();
        jedis.set(key, value.toString());
        jedis.close();

    }

    @Override
    public Provider get(String key) {

        Jedis jedis = _pool.getResource();
        String res =  jedis.get(key);
        jedis.close();
        return new Provider();
    }

    @Override
    public void txSetGet(Provider provider) {

        Jedis jedis = _pool.getResource();

        List<String> args = new ArrayList<>();
        args.add(provider.getFirstname());
        args.add(provider.getLastname());
        args.add(provider.getFacilityId());
        jedis.evalsha(_update_sh, new ArrayList<>(Arrays.asList(provider.getResourceId())),args);
        jedis.close();


    }


    public static void main(String[] args) {
        RedisClient jedisClient = new RedisClient("localhost", 6379);


        jedisClient.addProvider();
    }

    /**
     * example
     */
    public void addProvider()
    {

        String firstname = "john";
        String lastname = "davis";
        String providerId = UUID.randomUUID().toString();


        Jedis jedis = _pool.getResource();



        Pipeline pipeline  = jedis.pipelined();



        pipeline.hset(providerId,"first",firstname);
        pipeline.hset(providerId,"last",lastname);
        pipeline.hset(providerId,"PROF_TITLE", "oooooo");

        pipeline.sadd(firstname + "firstname_index",providerId);


        pipeline.zadd("firstnames_index", 0, firstname);

        pipeline.sync();


        jedis.hgetAll(providerId);
        jedis.hget(providerId,"first");

        Set<String> firstnames =  jedis.smembers("guy_firstname");

        Pipeline pipeline1 = jedis.pipelined();

        for (String resourceId: firstnames
             ) {
                pipeline.hgetAll(resourceId);
        }

        List<Object> results =  pipeline1.syncAndReturnAll();



                firstnames = jedis.zrangeByLex("firstname_index","[" + firstname,"[ZZZZZZZ",0,100);
        jedis.close();


    }


}
