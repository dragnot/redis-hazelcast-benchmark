package com.redislabs.competitive.benchmark;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
 * Created by guylubovitch on 12/12/16.
 */
public  class Utils {
    public static List<String> generalist(String filepath) {

        List<String> fileLines = new ArrayList<>();

        try (Stream<String> stream = Files.lines(Paths.get(filepath), StandardCharsets.ISO_8859_1)) {

            stream.forEach(fileLines::add);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return fileLines;
    }

    public static List<String> getSearchKeys(String KEY_PREFIX, String filePath) {


        List<String> lines = generalist(filePath);
        List<String> keys = new ArrayList<>(lines.size());

        for (String line :
                lines) {
            String[] items = line.split(",");
            if (items[0] != null)
                keys.add(KEY_PREFIX + items[0].trim());
        }    

        return keys;
    }
    public static List<String> getSearchFirst(String KEY_PREFIX, String filePath) {


        List<String> lines = generalist(filePath);
        List<String> keys = new ArrayList<>(lines.size());

        for (String line :
                lines) {
            String[] items = line.split(",");
            if (items[1] != null)
                keys.add(items[1].trim());
        }

        return keys;
    }

}
