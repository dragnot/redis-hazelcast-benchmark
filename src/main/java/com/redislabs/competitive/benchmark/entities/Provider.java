package com.redislabs.competitive.benchmark.entities;

import java.io.Serializable;
import java.util.Comparator;

/**
 * Created by guylubovitch on 12/14/16.
 */
public class Provider implements Serializable,Comparable<Provider> {

    private String resourceId;
    private String firstname;
    private String lastname;
    private String facilityId;

    public Provider() {
        //no instance
    }

    public Provider(String resourceId, String first,String last, String facilityId )
    {
        this.resourceId = resourceId;
        this.firstname = first;
        this.lastname = last;
        this.facilityId = facilityId;
    }

    public String getResourceId() {
        return resourceId;
    }

    public void setResourceId(String resourceId) {
        this.resourceId = resourceId;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFacilityId() {
        return facilityId;
    }

    public void setFacilityId(String facilityId) {
        this.facilityId = facilityId;
    }



    @Override
    public int compareTo(Provider provider) {

        if ( provider.getFirstname().equalsIgnoreCase(getFirstname()) )
            return 0;
        else
            return provider.getFirstname().compareTo(getFirstname());

    }
}
