package com.redislabs.competitive.benchmark.load;

import com.google.common.base.Stopwatch;
import com.redislabs.competitive.benchmark.KeyValueStore;
import com.redislabs.competitive.benchmark.Utils;
import com.redislabs.competitive.benchmark.clients.HazlecastClient;
import com.redislabs.competitive.benchmark.clients.RedisClient;
import com.redislabs.competitive.benchmark.entities.Provider;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

/**
 *
 * this class will just load specific data needed for the benchmark - a
 *  nd will prepare a set of keys for the benchmark as well
 * Created by guylubovitch on 12/8/16.
 */

public class BenchmarkLoader {

    private final String KEY_PREFIX ;
    private final String _host;
    private final int _port;
    private final int _iteration;
    private final String _filepath;
    private List<String> _fileLines = new ArrayList<>();
    private final String _db_name;


    public BenchmarkLoader() throws IOException {

        String propPath = System.getProperty( "application.properties" );
        Properties prop = new Properties();
        // load properties file
        prop.load(new FileInputStream(propPath));
        // get properties
        _host = prop.getProperty("host","localhost");
        _port = Integer.parseInt(prop.getProperty("port","6379"));
        _filepath = prop.getProperty("file","/Users/guylubovitch/Dropbox/redislabds/customers/kaiser/SampleData.csv");
        _iteration = Integer.parseInt(prop.getProperty("iteration", "1000"));
        _db_name = prop.getProperty("db_name","redis");
        KEY_PREFIX = prop.getProperty("prefix","bench");



    }

    public static void main(String[] args) throws IOException {


            BenchmarkLoader benchmarkLoader = new BenchmarkLoader();

            benchmarkLoader.loadJsonToDB();



    }

    public void loadJsonToDB()
    {



        KeyValueStore db;


        if ( _db_name.equalsIgnoreCase("redis") )
            db = new RedisClient(_host,_port);
        else
            db = new HazlecastClient(_host,_port);


        AtomicInteger counter;
        counter = new AtomicInteger();
        AtomicLong totalCounter = new AtomicLong();

        _fileLines =  Utils.generalist(_filepath);

        Stopwatch stopwatch = Stopwatch.createStarted();
        List<Thread> threads = new ArrayList<>();
        final CountDownLatch latch = new CountDownLatch(_iteration);

        for (int i = 0; i < _iteration; i++) {

            Runnable runnable = () -> {

                    //System.out.println("Item : " + line );
                List<String> list = Collections.synchronizedList(_fileLines);
                int threadIterator = counter.incrementAndGet();

                for (String aList : list) {

                    Provider provider = new Provider();
                    String[] items = aList.split(",");
                    if (items[0] != null || items[1] != null || items[3] != null) {
                        provider.setFirstname(items[1].trim());
                        provider.setLastname(items[3].trim());
                        provider.setResourceId(KEY_PREFIX +  items[0].trim() + threadIterator);
                    }
                    if ( items.length > 5 || items[6] != null )
                    {
                        provider.setFacilityId(items[6].trim());
                    }

                    db.add(provider);

                    totalCounter.incrementAndGet();
                }

                System.out.println( "number of items in db " + db.size()  );

                latch.countDown();

            };

            Thread thread = new Thread(runnable);
            thread.start();

            threads.add(thread);


        }

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        stopwatch.stop();

        System.out.println("Completed to load all data in " + stopwatch.elapsed(TimeUnit.SECONDS) + " loaded number of items " + counter.get());


    }

}
