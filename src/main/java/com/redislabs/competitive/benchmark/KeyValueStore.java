package com.redislabs.competitive.benchmark;

import com.redislabs.competitive.benchmark.entities.Provider;

import java.util.List;

/**
 * Created by Guy on 12/14/2014.
 */
public interface KeyValueStore {

    void set(final String key, final Provider value);
    Provider get(final String key);
//    void setwait(final String key, final String value);
//    String getwait(final String key);
//    void hset(final String key, final String value);
//    void hmset(final String key,final String[] keys, final String[] values);
//    String hget(final String key);
//    List<String> mGet(String[] keys);
//    void mSet(String[] keys, String[] values);
    void txSetGet(Provider provider);
//    void zadd( String key,double score,String value);
    void add(final Provider provider);
    Provider getMatchingFirst(final String first);
    Provider getTextMatchFirst(final String first_chars);
    long size();
    void flush();


}
